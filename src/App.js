import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };

  deleteTodo = id => {
    const newTodos = this.state.todos.filter(todo => todo.id !== id);
    this.setState({todos: newTodos});
  }

  getById = id => {
    for (let i in this.state.todos) if (this.state.todos[i].id === id) return i;
  }

  toggleCompleted = id => {
    const todo = this.getById(id);
    const newTodos = this.state.todos;
    newTodos[todo].completed = !newTodos[todo].completed;
    this.setState({todos: newTodos});
  }

  clearTodos = () => {
    const newTodos = this.state.todos.filter(todo => todo.completed === false);
    this.setState({todos: newTodos});
  }

  testInput = (e) => {
    if (e.keyCode === 13 && e.target.value.length > 0) {
      this.addTodo(e.target.value);
      e.target.value = "";
    }
  }

  addTodo = text => {
    const newTodos = this.state.todos;
    newTodos.push({ userId: 1, id: newTodos.length + 1, title: text, completed: false });
    this.setState({todos: newTodos});
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" onKeyUp={e => this.testInput(e)} autofocus />
        </header>
        <TodoList todos={this.state.todos} delFunc={this.deleteTodo.bind(this)} compFunc={this.toggleCompleted.bind(this)} />
        <footer className="footer">
          <span className="todo-count">
            <span style={{fontWeight: 'bold'}}>0</span> item(s) left
          </span>
          <button className="clear-completed" onClick={() => this.clearTodos()}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed} onClick={() => this.props.compFunc(this.props.id)} />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={() => this.props.delFunc(this.props.id)} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title} id={todo.id} completed={todo.completed} delFunc={this.props.delFunc} compFunc={this.props.compFunc} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
